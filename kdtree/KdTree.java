//package kdtree;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.Point2D;

import java.util.*;
/**
 * Created by User on 11/22/2015.
 */
public class KdTree {
    private class Item {
        Point2D p;
        Item l;
        Item r;
        boolean x;
        public Item(Point2D p) {
            this.p = p;
        }

        public Item(Point2D p, boolean x) {
            this.p = p;
            this.x = x;
        }
    }
    private int n = 0;
    private Item head;
    public KdTree() {}                              // construct an empty set of point
    public           boolean isEmpty() {                      // is the set empty?
        return n == 0;
    }
    public               int size() {                         // number of points in the set
        return n;
    }
    public              void insert(Point2D p) {              // add the point to the set (if it is not already in the set)
        if (n == 0) {
            head = new Item(p, true);
        }
        else {
            Item i = head;
            //Item
            while (i != null) {
                if (i.p.equals(p)) {
                    return;
                }
                if(i.x ? i.p.x() > p.x() : i.p.y() > p.y()) { // if left
                    if (i.l == null) {
                        i.l = new Item(p, !i.x);
                        break;
                    }
                    else {
                        i = i.l;
                    }

                }
                else {
                    if (i.r == null) {
                        i.r = new Item(p, !i.x);
                        break;
                    }
                    else {
                        i = i.r;
                    }
                }
            }
        }
        n++;
    }
    public           boolean contains(Point2D p) {            // does the set contain point p?
        Item i = head;
        while (i != null && !i.p.equals(p)) {
            if (i.x ? i.p.x() > p.x() : i.p.y() > p.y()) {
                i = i.l;
            }
            else {
                i = i.r;
            }
        }

        return i != null;
    }
    public              void draw() {                         // draw all points to standard draw
    }
    private void range(RectHV rect, Item i, LinkedList res) {
        if ((i.x ? rect.xmin() < i.p.x() : rect.ymin() < i.p.y()) && i.l != null) {
            range(rect, i.l, res);
        }
        if ((i.x ? rect.xmax() >= i.p.x() : rect.ymax() >= i.p.y()) && i.r != null) {
            range(rect, i.r, res);
        }
        if (rect.contains(i.p)) {
            res.add(i.p);
        }
    }
    public Iterable<Point2D> range(RectHV rect) {             // all points that are inside the rectangle
        LinkedList<Point2D> res = new LinkedList<>();
        if (n > 0) {
            range(rect, head, res);
        }
        return res;
    }
    private class NearResult {
        Point2D p;
        double dist;
    }
    private boolean fl;
    private double px;
    private double py;
    private void nearest(Point2D p, NearResult res, Item i) {
        while (i != null) {
            if (i.p.distanceTo(p) < res.dist) {
                res.p = i.p;
                res.dist = res.p.distanceTo(p);
                if (i.x ? p.x() < i.p.x() : p.y() < i.p.y()) {
                    nearest(p, res, i.l);
                    i = i.r; //nearest(p, res, i.r);
                }
                else {
                    nearest(p, res, i.r);
                    i = i.l; //nearest(p, res, i.r);
                }
            } else {

                fl = i.x ? p.x() + res.dist >= i.p.x() : p.y() + res.dist >= i.p.y();
                if (i.x ? p.x() - res.dist < i.p.x() : p.y() - res.dist < i.p.y()) {
                    if (fl) {
                        if (i.x ? p.x() < i.p.x() : p.y() < i.p.y()) {
                            nearest(p, res, i.l);
                            i = i.r; //nearest(p, res, i.r);
                        }
                        else {
                            nearest(p, res, i.r);
                            i = i.l; //nearest(p, res, i.r);
                        }
                    }
                    else {
                        i = i.l;
                    }
                }
                else {
                    if (fl) {
                        i = i.r;
                    }
                }
            }
        }
    }
    public           Point2D nearest(Point2D p) {             // a nearest neighbor in the set to point p; null if the set is empty
        if (n > 0) {
            NearResult res = new NearResult();
            res.p = head.p;
            res.dist = head.p.distanceTo(p);
            nearest(p, res, head);
            return res.p;
        }
        else {
            return null;
        }
    }
    public static void main(String[] args){                  // unit testing of the methods (optional)}
        int N = 1000;
        Random rand = new Random();
        KdTree kd = new KdTree();
        PointSET ps = new PointSET();
        ArrayList<Point2D> array = new ArrayList<>();
        for (int i = 0; i < 640000; i++) {
            Point2D p = new Point2D(rand.nextDouble(), rand.nextDouble());
            kd.insert(p);
            kd.insert(p);
            ps.insert(p);
            array.add(p);
            if(kd.size() != ps.size() || kd.contains(p) != ps.contains(p)) {
                System.out.println("Err " + kd.size() +  " " + ps.size());
            }
        }
        for (int i = 0; i < 1; i++) {
            Point2D p = new Point2D(rand.nextDouble(), rand.nextDouble());
            if(!kd.nearest(p).equals(ps.nearest(p))) {
                System.out.println("Nearest " + " ");
            }
        }
    }
}
