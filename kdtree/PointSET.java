//package kdtree;

import edu.princeton.cs.algs4.RectHV;

import java.util.*;

import edu.princeton.cs.algs4.Point2D;
/**
 * Created by User on 11/22/2015.
 */
public class PointSET {
    private TreeSet<Point2D> d = new TreeSet<>();
    public PointSET() {                              // construct an empty set of points
    }

    public           boolean isEmpty() {                      // is the set empty?
        return d.size() == 0;
    }
    public               int size() {                         // number of points in the set
        return d.size();
    }
    public              void insert(Point2D p) {              // add the point to the set (if it is not already in the set)
        d.add(p);
    }
    public           boolean contains(Point2D p) {            // does the set contain point p?
        return d.contains(p);
    }
    public              void draw() {                         // draw all points to standard draw
    }
    public Iterable<Point2D> range(RectHV rect) {             // all points that are inside the rectangle
        LinkedList<Point2D> res = new LinkedList<>();
        for (Point2D p : d) {
            if (rect.contains(p)) {
                res.add(p);
            }
        }
        return res;
    }
    public           Point2D nearest(Point2D p) {             // a nearest neighbor in the set to point p; null if the set is empty
        Point2D res = null;
        for (Point2D i : d) {
            if (res == null) {
                res = i;
            }
            else {
                if (res.distanceTo(p) > i.distanceTo(p)) {
                    res = i;
                }
            }
        }
        return res;
    }

    public static void main(String[] args){                  // unit testing of the methods (optional)}
        Random rand = new Random();
        PointSET kd = new PointSET();
        System.out.println(kd.size());
        for (int i = 0; i < 1000000; i++) {
            kd.insert(new Point2D(rand.nextDouble(), rand.nextDouble()));
        }
    }
}
