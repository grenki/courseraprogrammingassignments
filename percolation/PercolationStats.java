package percolation;

import java.util.Random;

/**
 * Created by User on 10/30/2015.
 */

public class PercolationStats {

    private double mean = 0;                   // sample mean of percolation threshold
    private double stddev = 0;                    // sample standard deviation of percolation threshold
    private double confidenceLo;              // low  endpoint of 95% confidence interval
    private double confidenceHi;              // high endpoint of 95% confidence interval

    public PercolationStats(int N, int T)     // perform T independent experiments on an N-by-N grid
    {
        if (N <= 0 || T <= 0) {
            throw new IllegalArgumentException();
        }
        double[] x  = new double[T];
        int N2 = N*N;
        for (int i = 0; i < T; i++) {
            Percolation p = new Percolation(N);
            Random random = new Random();
            while (!p.percolates()) {
                int i1 = random.nextInt(N) + 1;
                int j1 = random.nextInt(N) + 1;
                if (!p.isOpen(i1, j1)) {
                    p.open(i1, j1);
                    x[i]++;
                }
            }
            x[i] /= N2;
            mean += x[i];
        }
        mean /= T;

        for (int i = 0; i < T; i++) {
            stddev += (x[i] - mean) * (x[i] - mean);
        }
        stddev = Math.sqrt(stddev / (T - 1));
        double interval = 1.96 * stddev / Math.sqrt(T);
        confidenceLo = mean - interval;
        confidenceHi = mean + interval;
    }

    public double mean() {
        return mean;
    }

    public double stddev() {
        return stddev;
    }

    public double confidenceLo() {
        return confidenceLo;
    }

    public double confidenceHi() {
        return confidenceHi;
    }

    public static void main(String[] args)    // test client (described below)
    {
        PercolationStats ps = new PercolationStats(200, 100);
        System.out.println(ps.mean + "\n" + ps.stddev  + "\n" + ps.confidenceLo  + "\n" + ps.confidenceHi);

    }
}