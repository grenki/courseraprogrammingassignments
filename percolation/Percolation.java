package percolation;

import edu.princeton.cs.algs4.WeightedQuickUnionUF;

import java.util.LinkedList;

/**
 * Created by User on 10/27/2015.
 */
public class Percolation {
    private int[][] map;
    private int n;
    private int openedSites = 0;
    private boolean isPercolate = false;
    private WeightedQuickUnionUF uf;


    public Percolation(int N) { // create N-by-N grid, with all sites blocked
        if (N <= 0) {
            throw new IllegalArgumentException();
        }
        map = new int[N+2][N+2];
        uf = new WeightedQuickUnionUF(N+5);
        n = N;
        for (int i = 0; i < n + 2; ++i) {
            map[0][i] = 2;
        }
    }

    private class Point {
        private int x;
        private int y;
        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    private int getOpenedSites() {
        return openedSites;
    }

    private void neigbours(LinkedList<Point> res, int value, int i, int j, boolean change) {
        for (int i1 = -1; i1 < 2; i1++) {
            for (int j1 = -1; j1 < 2; j1++) {
                if ((Math.abs(i1) + Math.abs(j1) == 1) && map[i+i1][j+j1] == value) {
                    res.add(new Point(i+i1, j+j1));
                    if (change) {
                        map[i + i1][j + j1] = 2;
                    }
                }
            }
        }
    }

    private void notOutOfBounds(int i, int j) {
        if (!(i <= n && j <= n && i > 0 && j > 0)) {
            throw new IndexOutOfBoundsException();
        }
    }
    private void fullPoint(int i, int j) {
        if (i == n) {
            isPercolate = true;
        }
        LinkedList<Point> list = new LinkedList<>();
        list.add(new Point(i, j));
        map[i][j] = 2;
        Point p;
        while (list.size() > 0) {
            p = list.poll();
            neigbours(list, 1, p.x, p.y, true);
        }

    }
    public void open(int i, int j)          // open site (row i, column j) if it is not open already
    {
        notOutOfBounds(i, j);
        uf.union(i, j);
        uf.find(i);
        uf.find(j);
        if (map[i][j] == 0) {
            map[i][j] = 1;
            LinkedList<Point> tmp = new LinkedList<>();
            neigbours(tmp, 2, i, j, false);
            if (tmp.size() > 0){
                fullPoint(i, j);
            }
        }
    }

    public boolean isOpen(int i, int j)     // is site (row i, column j) open?
    {
        notOutOfBounds(i, j);
        return map[i][j] > 0;
    }

    public boolean isFull(int i, int j)     // is site (row i, column j) full?
    {
        notOutOfBounds(i, j);
        return map[i][j] == 2;
    }

    public boolean percolates()             // does the system percolate?
    {
        for (int i = 1; i < n+1 && !isPercolate; i++) {
            isPercolate = isPercolate || map[n][i] == 2;
        }
        return isPercolate;
    }

    private void print() {
        for (int i = 0; i < n + 2; i++) {
            for(int j = 0; j < n + 2; j++) {
                System.out.print(map[i][j] + " ");
            }
            System.out.println();
        }
    }
    private void printFull() {
        System.out.println("Full");
        for (int i = 1; i < n + 1; i++) {
            for (int j = 1; j < n + 1; j++) {
                System.out.print(isFull(i,j) + " ");
            }
            System.out.println();
        }
        System.out.println();
    }
    private void printOpen() {
        System.out.println("Open");
        for (int i = 1; i < n + 1; i++) {
            for (int j = 1; j < n + 1; j++) {
                System.out.print(isOpen(i, j) + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void main(String[] args){
        Percolation p = new Percolation(4);
        System.out.println(p.percolates());

        p.printOpen();
        p.open(1, 4);
        p.open(2, 4);
        p.open(4, 4);
        p.open(4, 3);
        p.open(4, 1);
        p.printFull();
        p.printOpen();
        p.open(3, 4);
        p.printOpen();
        p.printFull();
        System.out.println("open 1 4 " + p.isOpen(4, 1));
        System.out.println(p.isFull(1, 4));
        System.out.println(p.isOpen(1, 4));
        System.out.println(p.isFull(4, 4));
        System.out.println("percolates " + p.percolates());
    }
}
