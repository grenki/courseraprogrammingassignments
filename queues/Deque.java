package queues;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.Consumer;

public class Deque<Item> implements Iterable<Item> {
    private Element head;
    private Element tail;
    private int size;
    private class Element {
        Item data;
        Element next;
        Element last;
        public Element(Item data) {
            this.data = data;
        }
    }
    public Deque() { }                          // construct an empty deque
    private void notNull(Item item) {
        if (item == null) {
            throw new NullPointerException();
        }
    }
    private void notEmpty() {
        if (size == 0){
            throw new NoSuchElementException();
        }
    }
    public boolean isEmpty() {                 // is the deque empty?
        return size == 0;
    }
    public int size() {                        // return the number of items on the deque
        return size;
    }
    public void addFirst(Item item)          // add the item to the front
    {
        notNull(item);
        Element e = new Element(item);
        if (size == 0) {
            head = e;
            tail = e;
        }
        else {
            head.last = e;
            e.next = head;
            head = e;
        }

        size++;
    }
    public void addLast(Item item)           // add the item to the end
    {
        notNull(item);
        Element e = new Element(item);
        if (size == 0) {
            head = e;
            tail = e;
        }
        else {
            tail.next = e;
            e.last = tail;
            tail = e;
        }

        size++;
    }
    public Item removeFirst()                // remove and return the item from the front
    {
        notEmpty();
        Item item = head.data;
        head = head.next;
        if (size == 1){
            tail = null;
            head = null;
        }
        else {
            head.last = null;
        }
        size--;
        return item;
    }
    public Item removeLast()                 // remove and return the item from the end
    {
        notEmpty();
        Item data = tail.data;
        tail = tail.last;
        if (size == 1){
            tail = null;
            head = null;
        }
        else {
            tail.next = null;
        }
        size--;
        return data;
    }
    public Iterator<Item> iterator()         // return an iterator over items in order from front to end
    {
        return new Iterator<Item>(){
            Element e = head;
            @Override
            public boolean hasNext() {
                return e != null;
            }
            @Override
            public Item next() {
                if (e == null){
                    throw new NoSuchElementException();
                }
                Item item = e.data;
                e = e.next;
                return item;
            }
        };
    }
    public static void main(String[] args)   // unit testing
    {
        Deque<Integer> d = new Deque<>();
        d.addLast(2);
        Iterator<Integer> j = d.iterator();
        while (j.hasNext()) {
            System.out.println(j.next());
        }
        System.out.println();
        for (int i = 0; i < 10; i++) {
            d.addFirst(i);
        }
        //System.out.println("size " + d.size());
        Iterator<Integer> i = d.iterator();
        while (i.hasNext()) {
            System.out.println(i.next());
        }
        //System.out.println(d.removeLast());
       // System.out.println(d.removeFirst());


    }
}