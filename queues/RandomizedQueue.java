package queues;

import edu.princeton.cs.algs4.StdRandom;

import java.util.Iterator;
import java.util.Random;

/**
 * Created by User on 11/04/2015.
 */
public class RandomizedQueue<Item> implements Iterable<Item> {
    private int n = 0;
    private int capacity = 1;
    private Item[] data = (Item[]) new Object[capacity];
    private Random rand = new Random();
    private void notEmpty() {
        if (n == 0) {
            throw new java.util.NoSuchElementException();
        }
    }
    public RandomizedQueue() {}               // construct an empty randomized queue
    public boolean isEmpty() { return n == 0; }                // is the queue empty?
    public int size() { return n; }                        // return the number of items on the queue
    public void enqueue(Item item)           // add the item
    {
        if (item == null){
            throw new java.lang.NullPointerException();
        }
        if (n == capacity){
            capacity *= 2;
            Item[] newData = (Item[]) new Object[capacity];
            for (int i = 0; i < n; i++) {
                newData[i] = data[i];
            }
            data = newData;
        }

        data[n++] = item;
    }
    public Item dequeue()                    // remove and return a random item
    {
        notEmpty();
        int i = rand.nextInt(n);
        Item item = data[i];
        data[i] = data[--n];
        data[n] = null;

        if (n < capacity / 4){
            capacity /= 2;
            Item[] newData = (Item[]) new Object[capacity];
            for (int j = 0; j < n; j++) {
                newData[j] = data[j];
            }
            data = newData;
        }
        return item;
    }
    public Item sample()                     // return (but do not remove) a random item
    {
        notEmpty();
        return data[rand.nextInt(n)];
    }
    public Iterator<Item> iterator()         // return an independent iterator over items in random order[
    {
        return new Iterator<Item>() {
            int count = 0;
            final int[] order = new int[n];
            {
                for (int i = 0; i < n; i++) {
                    order[i] = i;
                }
                StdRandom.shuffle(order);
            }
            @Override
            public boolean hasNext() {
                return count < n;
            }
            @Override
            public Item next() {
                if (count == n){
                    throw new java.util.NoSuchElementException();
                }
                return data[order[count++]];
            }
        };
    }

    public static void main(String[] args)   // unit testing
    {

        RandomizedQueue<Integer> rq = new RandomizedQueue<>();
        for (int i = 0; i < 10; i++) {
            rq.enqueue(i % 10);
            //System.out.println("size = "  + rq.size());
        }

        int[] stat = new int[10];
        for (int i = 0; i < 10; i++) {
            Iterator<Integer> iter = rq.iterator();
            stat[iter.next()]++;
        }
        for (int i = 0; i < stat.length; i++) {
            System.out.println(stat[i]);

        }
        System.out.println();
    }

}
