//package burrows;
import java.util.Arrays;
import java.util.Comparator;

public class CircularSuffixArray {
    private Integer[] res;
    int n;
    public CircularSuffixArray(String s) { // circular suffix array of s
        if  (s == null) {
            throw new NullPointerException();
        }

        n = s.length();
        res = new Integer[n];
        for (int i = 0; i < n; i++) {
            res[i] = i;
        }

        Arrays.parallelSort(res, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                for (int i = 0; i < n; i++) {

                    if (s.charAt(o1) == s.charAt(o2)) {
                        o1 = (o1 + 1) % n;
                        o2 = (o2 + 1) % n;
                    }
                    else  if (s.charAt(o1) < s.charAt(o2)){
                        return -1;
                    }
                    else {
                        return 1;
                    }
                }
                return 0;
            }
        });
    }
    public int length() {             // length of s
        return n;
    }
    public int index(int i) {              // returns index of ith sorted suffix
        return res[i];
    }
    public static void main(String[] args) {    // unit testing of the methods (optional)
    }
}