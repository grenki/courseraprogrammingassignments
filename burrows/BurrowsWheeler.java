//package burrows;
import edu.princeton.cs.algs4.BinaryStdIn;
import edu.princeton.cs.algs4.BinaryStdOut;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;

public class BurrowsWheeler {
    // apply Burrows-Wheeler encoding, reading from standard input and writing to standard output
    public static void encode() {
        String s ;
        s = BinaryStdIn.readString();
        CircularSuffixArray m = new CircularSuffixArray(s);
        int k = -1;
        int n = s.length();
        for (int i = 0; i < n; i++) {
            if (m.index(i) == 0) {
                k = i;
                break;
            }
        }
        BinaryStdOut.write(k);
        for (int i = 0; i < n; i++) {
            BinaryStdOut.write(s.charAt((m.index(i) - 1 + n) % n));
        }
        BinaryStdOut.close();
    }

    // apply Burrows-Wheeler decoding, reading from standard input and writing to standard output
    public static void decode() {
        int first;
        first = BinaryStdIn.readInt();
        String s;
        s = BinaryStdIn.readString();
        int n = s.length();
        char[] t = new char[n];
        char[] fch = new char[n];
        int[] next = new int[n];
        for (int i = 0; i < n; i++) {
            t[i] = s.charAt(i);
            fch[i] = s.charAt(i);
        }
        Arrays.parallelSort(fch);

        HashMap<Character, ArrayList<Integer>> map = new HashMap<>();
        for (int i = n - 1; i >= 0; i--) {
            if (!map.containsKey(t[i])) {
                map.put(t[i], new ArrayList<>());
            }
            map.get(t[i]).add(i);
        }
        for (int i = 0; i < n; i++) {
            next[i] = map.get(fch[i]).remove(map.get(fch[i]).size()-1);
        }
        int k = first;
        for (int i = 0; i < n; i++) {
            BinaryStdOut.write(fch[k]);
            k = next[k];
        }
        BinaryStdOut.close();
    }
    // if args[0] is '-', apply Burrows-Wheeler encoding
    // if args[0] is '+', apply Burrows-Wheeler decoding
    public static void main(String[] args) {
        if (args.length == 0) {
           // encode();
            decode();
        }
        else {
            if (args[0] == "-") {
                encode();
            }
            if (args[0] == "+") {
                decode();
            }
        }
    }
}
