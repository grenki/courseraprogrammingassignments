//package burrows;
import edu.princeton.cs.algs4.BinaryStdIn;
import edu.princeton.cs.algs4.BinaryStdOut;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 * Created by User on 12/06/2015.
 */
public class MoveToFront {
    // apply move-to-front encoding, reading from standard input and writing to standard output
    private static final int n = 256;
    public static void encode() {
        LinkedList<Character> alp = new LinkedList<>();
        for (int i = 0; i < n; i++) {
            alp.add((char) i);
        }
        while (!BinaryStdIn.isEmpty()) {
            char c = BinaryStdIn.readChar();
            ListIterator<Character> iter = alp.listIterator();
            byte res = 0;
            while(iter.hasNext()) {
                if (iter.next() == c) {
                    iter.remove();
                    alp.addFirst(c);
                    //System.out.println(res + " ");
                    BinaryStdOut.write(res);
                    break;
                }
                res++;
            }
        }
        BinaryStdOut.close();
    }


    // apply move-to-front decoding, reading from standard input and writing to standard output
    public static void decode() {
        LinkedList<Character> alp = new LinkedList<>();
        for (int i = 0; i < n; i++) {
            alp.add((char) i);
        }
        while (!BinaryStdIn.isEmpty()) {
            byte c = BinaryStdIn.readByte();
            ListIterator<Character> iter = alp.listIterator();
            byte res = 0;
            while(iter.hasNext()) {
                char ch = iter. next();
                if (res == c) {
                    iter.remove();
                    alp.addFirst(ch);
                    //System.out.println(res + " ");
                    BinaryStdOut.write(ch);
                    break;
                }
                res++;
            }
        }
        BinaryStdOut.close();
    }

    // if args[0] is '-', apply move-to-front encoding
    // if args[0] is '+', apply move-to-front decoding
    public static void main(String[] args) {
        if (args.length == 0) {
            encode();
        }
        else {
            if (args[0] == "-") {
                encode();
            }
            if (args[0] == "+") {
                decode();
            }
        }
    }
}
