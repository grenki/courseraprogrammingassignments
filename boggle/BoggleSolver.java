package boggle;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;
import java.util.HashSet;
import java.util.LinkedList;

public class BoggleSolver
{
    private int[] score = {0, 0, 0, 1, 1, 2, 3, 5};
    private HashSet<String> dict = new HashSet<>();
    private Item head = new Item();

    private class Item {
        char c;
        Item[] a = new Item[26];
        Item parent;
        boolean isEnd = false;

        public Item(char c) {
            this.c = c;
        }
        public Item(char c, Item parent) {
            this.c = c;
            this.parent = parent;
        }
        public Item() {
        }
    }

    // Initializes the data structure using the given array of strings as the dictionary.
    // (You can assume each word in the dictionary contains only the uppercase letters A through Z.)
    public BoggleSolver(String[] dictionary) {
        for (int i = 0; i < dictionary.length; i++) {
            String s = dictionary[i];
            if (s.length() < 3) {
                continue;
            }

            dict.add(s);
            Item item = head;
            for (int j = 0; j < s.length(); j++) {
                char c = s.charAt(j);
                if (c == 'Q') {
                    j++;
                }
                if (item.a[c-'A'] == null) {
                    item.a[c-'A'] = new Item(c, item);
                }
                item = item.a[c - 'A'];
            }
            item.isEnd = true;
        }
    }

    // Returns the set of all valid words in the given Boggle board, as an Iterable.
    private class Pair{
        int i;
        int j;
        Item item;

        public Pair(int i, int j) {
            this.i = i;
            this.j = j;
        }

        public Pair(int i, int j, Item it) {
            this.i = i;
            this.j = j;
            this.item = it;

        }
        public boolean eq (Pair p) {
            return p != null && p.i == i && p.j == j;
        }
    }
    public Iterable<String> getAllValidWords(BoggleBoard b) {
        HashSet<String> words = new HashSet<>();
        int n = b.rows();
        int m = b.cols();
        LinkedList<Pair> stack = new LinkedList<>();
        LinkedList<Pair> path = new LinkedList<>();
        LinkedList<Pair> parent = new LinkedList<>();
        int[][] be = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                Item item = head.a[b.getLetter(i,j) - 'A'];
                if (item != null) {
                    stack.add(new Pair(i, j, item));
                    parent.add(null);
                }
            }
        }

        while (stack.size() > 0) {
            Pair p = stack.pollLast();
            Pair parentP = parent.pollLast();
            if (parentP != null) {
                Pair p2 = path.pollLast();
                while (p2 != null && !parentP.eq(p2)) {
                    be[p2.i][p2.j] = 0;
                    p2 = path.pollLast();
                }
                path.addLast(parentP);
                path.addLast(p);
                be[p.i][p.j] = 1;
            }
            else {
                for (int i = 0; i < n; i++) {
                    for (int j = 0; j < m; j++) {
                        be[i][j]=0;
                    }
                }
                be[p.i][p.j] = 1;
                path.clear();
                path.add(p);
            }
            if (p.item.isEnd) {
                String s = "";
                Item it = p.item;
                while (it.parent != null) {
                    s = it.c == 'Q' ? "QU" + s :it.c + s;
                    it = it.parent;
                }
                if (dict.contains(s)) {
                    words.add(s);
                }
            }
            for (int i = -1; i < 2; i++) {
                for (int j = -1; j < 2; j++) {
                    if (i * i + j *j != 0 && p.i + i >= 0 && p.i + i < n && p.j + j >= 0 && p.j + j < m) {
                        Pair son = new Pair(i + p.i, j + p.j);
                        if (be[son.i][son.j] == 0) {
                            char c = b.getLetter(son.i, son.j);
                            if (p.item.a[c - 'A'] != null) {
                                son.item = p.item.a[c - 'A'];
                                stack.addLast(son);
                                parent.addLast(p);
                            }
                        }
                    }
                }
            }
        }
        return words;
    }

    // Returns the score of the given word if it is in the dictionary, zero otherwise.
    // (You can assume the word contains only the uppercase letters A through Z.)
    public int scoreOf(String word) {
        if (!dict.contains(word)) {
            return 0;
        }
        if (word.length() >= 8) {
            return 11;
        }
        else {
            return score[word.length()];
        }
    }

    public static void main(String[] args) {
        In in = new In("dictionary-yawl.txt");
        String[] dictionary = in.readAllStrings();
        BoggleSolver solver = new BoggleSolver(dictionary);
        BoggleBoard board = new BoggleBoard("board-inconsequentially.txt");
        System.out.println(board);
        int score = 0;
        int count = 0;
        for (String word : solver.getAllValidWords(board))
        {
            StdOut.println(word);
            score += solver.scoreOf(word);
            count ++;
            //System.out.println(score);
        }
        System.out.println(count);
        StdOut.println("Score = " + score);
    }
}