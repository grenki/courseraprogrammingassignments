package boggle;

import edu.princeton.cs.algs4.In;
import java.util.Random;

public class BoggleBoard
{
    private int n;
    private int m;
    private char a[][];
    // Initializes a random 4-by-4 Boggle board.
    // (by rolling the Hasbro dice)
    public BoggleBoard() {
        m = 4;
        n = 4;
        a = new char[n][m];
        randomFilling();
    }

    // Initializes a random M-by-N Boggle board.
    // (using the frequency of letters in the English language)
    public BoggleBoard(int M, int N) {
        m = M;
        n = N;
        a = new char[n][m];
        randomFilling();
    }
    private void randomFilling() {
        Random rand = new Random();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                a[i][j] = (char)(rand.nextInt(27) + 'A');
            }
        }
    }
    // Initializes a Boggle board from the specified filename.
    public BoggleBoard(String filename) {
        In in = new In(filename);
        n = in.readInt();
        m = in.readInt();
        a = new char[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                a[i][j] = in.readString().charAt(0);
            }
        }
    }

    // Initializes a Boggle board from the 2d char array.
    // (with 'Q' representing the two-letter sequence "Qu")
    public BoggleBoard(char[][] A) {
        n = a.length;
        m = a[0].length;
        a = new char[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                a[i][j] = A[i][j];
            }
        }
    }

    // Returns the number of rows.
    public int rows() {
        return n;
    }

    // Returns the number of columns.
    public int cols() {
        return m;
    }

    // Returns the letter in row i and column j.
    // (with 'Q' representing the two-letter sequence "Qu")
    public char getLetter(int i, int j) {
        return a[i][j];
    }
    // Returns a string representation of the board.
    public String toString() {
        String s = "";
        s += n + " " + m + "\n";
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                s += a[i][j] + " ";
            }
            s += "\n";
        }

        return s;
    }

    public static void main(String[] args) {
        BoggleBoard b = new BoggleBoard("board-points5.txt");
        System.out.println(b);
    }
}