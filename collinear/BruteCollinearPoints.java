package collinear;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;
import javafx.util.Pair;

import java.util.*;

/**
 * Created by User on 11/04/2015.
 */
public class BruteCollinearPoints {
    //private Point[] points;
    private int n;
    private int numberOfSegments = 0;
    private LinkedList<Line> segmentsList = new LinkedList<>();
    private LineSegment[] segments;

    private class Line {
        public Line(Point p, Point q) {
            this.p = p;
            this.q = q;
        }

        Point p;
        Point q;
    }

    private boolean check(Point a, Point b, Point c, Point d) {
        if (compare(a, b, c, d)) {
            Point min = min(a, b, c, d, false);
            Point max = min(a, b, c, d, true);
            Line line = new Line(min, max);

            boolean fl = true;
            for (int i = 0; fl && i < segmentsList.size(); i++) {
                Line l2 = segmentsList.get(i);
                if (compare(line, l2)) {

                    l2.p = min(min, l2.p, false);
                    l2.q = min(max, l2.q, true);
                    fl = false;
                }
            }

            if (fl) {
                segmentsList.add(new Line(min, max));
            }
            return fl;
        }
        return false;
    }

    private boolean compare(Point a, Point b, Point c, Point d) {
        return a.slopeOrder().compare(b, c) == 0 && d.slopeOrder().compare(b, c) == 0;
    }

    private boolean compare(Line a, Line b) {
        if((a.p == b.p || a.q == b.q) && a.p.slopeTo(a.q) == b.p.slopeTo(b.q)){
            return true;
        }
        return a.p.slopeOrder().compare(b.p, b.q) == 0 && a.q.slopeOrder().compare(b.p, b.q) == 0;
    }

    private Point min(Point p, Point q, Point p1, Point q1, boolean max) {
        return min(min(p, q, max), min(p1, q1, max), max);
    }

    private Point min(Point p, Point q, boolean max) {
        return p.compareTo(q) < 0 ^ max ? p : q;
    }
    private void checkEquals(Point[] p) {
        for (int i = 0; i < p.length; i++) {
            for (int j = i + 1; j < p.length; j++) {
                if (p[i].compareTo(p[j]) == 0){
                    throw new java.lang.IllegalArgumentException();
                }
            }
        }
    }
    public BruteCollinearPoints(Point[] ps) {   // finds all line segments containing 4
        n = ps.length;
        Point[] points = new Point[n];
        for (int i = 0; i < n; i++) {
            points[i] = ps[i];
        }
        checkEquals(points);
        Arrays.sort(points, new Comparator<Point>() {
            @Override
            public int compare(Point o1, Point o2) {
                return o1.compareTo(o2);
            }
        });
        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                for (int k = j + 1; k < n; k++) {
                    for (int l = k + 1; l < n; l++) {
                        if (check(points[i], points[j], points[k], points[l])) {
                            k = l + 1;
                        }
                    }
                }
            }
        }
        //System.out.println("sizeof = " + numberOfSegments);
        numberOfSegments = segmentsList.size();
        segments = new LineSegment[numberOfSegments];
        for (int i = 0; i < segmentsList.size(); i++) {
            Line line = segmentsList.get(i);
            segments[i] = new LineSegment(line.p, line.q);
        }
    }

    public int numberOfSegments() {      // the number of line segments
        return numberOfSegments;
    }

    public LineSegment[] segments() {               // the line segments
        return segments;
    }

    public static void main(String[] args) {

        // read the N points from a file
        System.out.println(args[0]);
        In in = new In("rand.txt");
        int N = in.readInt();
        Point[] points = new Point[N];
        for (int i = 0; i < N; i++) {
            int x = in.readInt();
            int y = in.readInt();
            points[i] = new Point(x, y);
            System.out.println(points[i]);
        }

        // draw the pointsvvvv
        StdDraw.show(0);
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        for (Point p : points) {
            p.draw();
        }
        StdDraw.show();
        System.out.println("drew");

        // print and draw the line segments
        BruteCollinearPoints collinear = new BruteCollinearPoints(points);
        for (LineSegment segment : collinear.segments()) {
            StdOut.println(segment);
            segment.draw();
        }

        collinear = new BruteCollinearPoints(points);
        for (LineSegment segment : collinear.segments()) {
            StdOut.println(segment);
            segment.draw();
        }
        System.out.println();
        System.out.println();

        for (int i = 0; i < points.length; i++) {
            System.out.println(points[i]);
        }

    }
}