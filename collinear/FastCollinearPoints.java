package collinear;

/**
 * Created by User on 11/04/2015.
 */
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;

public class FastCollinearPoints {
    private int n;
    private int numberOfSegments = 0;
    private LinkedList<Line> segmentsList = new LinkedList<>();
    private LineSegment[] segments;

    private class Line {
        public Line(Point p, Point q) {
            this.p = p;
            this.q = q;
        }

        Point p;
        Point q;
    }
    private int DoubleCompare(double a, double b){
        return a > b ? 1 : a == b ? 0 : -1;
    }
    private boolean compare(Line a, Line b) {
        if((a.p == b.p || a.q == b.q) && a.p.slopeTo(a.q) == b.p.slopeTo(b.q)){
            return true;
        }
        return a.p.slopeOrder().compare(b.p, b.q) == 0 && a.q.slopeOrder().compare(b.p, b.q) == 0;
    }
    private boolean notExist(Point a, Point b) {
        boolean fl = true;
        for (int k = 0; fl && k < segmentsList.size(); k++) {
            fl = fl && !compare(segmentsList.get(k), new Line(a, b));
        }

        return false;
    }
    private Point min(Point p, Point q, boolean max) {
        return p.compareTo(q) < 0 ^ max ? p : q;
    }
    private void checkEquals(Point[] p) {
        for (int i = 0; i < p.length; i++) {
            for (int j = i + 1; j < p.length; j++) {
                if (p[i].compareTo(p[j]) == 0){
                    throw new java.lang.IllegalArgumentException();
                }
            }
        }
    }
    public FastCollinearPoints(Point[] points)     // finds all line segments containing 4 or more points
    {
        checkEquals(points);
        n = points.length;
        Point[] ps = new Point[n];
        for (int i = 0; i < n; i++) {
            ps[i] = points[i];
        }
        for (int i = 0; i < n; i++) {
            final Point p = points[i];
            Arrays.sort(ps, new Comparator<Point>() {
                @Override
                public int compare(Point o1, Point o2) {
                    int c = DoubleCompare(p.slopeTo(o1), p.slopeTo(o2));
                    return c == 0 ? o1.compareTo(o2) : c;
                }
            });

            double slope=0;
            int previous = 0;
            for (int j = 0; j < n; j++) {
                Point q = ps[j];
                if (p.slopeTo(q) != slope && previous >= 3) {

                    if (notExist(p, ps[j-1])) {
                        segmentsList.add(new Line(min(p, ps[j - previous], false), min(p, ps[j - 1], true)));
                    }

                    slope = p.slopeTo(q);
                    previous = 1;
                }
                else {
                    previous++;
                }
            }


        }
        numberOfSegments = segmentsList.size();
        segments = new LineSegment[numberOfSegments];
        for (int i = 0; i < segmentsList.size(); i++) {
            Line line = segmentsList.get(i);
            segments[i] = new LineSegment(line.p, line.q);
        }
    }
    public int numberOfSegments() {       // the number of line segments
        return numberOfSegments;
    }
    public LineSegment[] segments() {
        return segments;
    }

    public static void main(String[] args) {

        // read the N points from a file
        In in = new In("rand.txt");
        int N = in.readInt();
        Point[] points = new Point[N];
        for (int i = 0; i < N; i++) {
            int x = in.readInt();
            int y = in.readInt();
            points[i] = new Point(x, y);
        }

        // draw the points
        StdDraw.show(0);
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        for (Point p : points) {
            p.draw();
        }
        StdDraw.show();

        // print and draw the line segments
        BruteCollinearPoints collinear = new BruteCollinearPoints(points);

        System.out.println("size " + collinear.segments().length);
        for (LineSegment segment : collinear.segments()) {
            StdOut.println(segment);
            segment.draw();
        }
    }
}
