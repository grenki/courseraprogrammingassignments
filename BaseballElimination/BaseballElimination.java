//package BaseballElimination;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public class BaseballElimination {
    private int n;
    private HashMap<String, Integer> s;
    private String[] names;
    private int[] w;
    private int[] l;
    private int[] r;
    private int[][] a;
    private int maxWins = 0;
    private int countMathces = 0;  // count Matches in division
    private ArrayList<LinkedList<String>> cache;

    public BaseballElimination(String filename) {                  // create a baseball division from given filename in format specified below
        In in = new In(filename);
        n = in.readInt();
        names = new String[n];
        s = new HashMap<>(n);
        w = new int[n];
        l = new int[n];
        r = new int[n];
        a = new int[n][n];
        for (int i = 0; i < n; i++) {
            names[i] = in.readString();
            s.put(names[i], i);
            w[i] = in.readInt();
            l[i] = in.readInt();
            r[i] = in.readInt();
            for (int j = 0; j < n; j++) {
                a[i][j] = in.readInt();
            }
        }
        for (int i = 0; i < n; i++) {
            maxWins = Math.max(maxWins, w[i]);
        }
        cache = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            cache.add(null);
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < i; j++) {
                if (a[i][j] > 0) {
                    countMathces++;
                }
            }
        }
    }
    private void validate(String team) {
        if (!s.containsKey(team)) {
            throw new IllegalArgumentException();
        }
    }
    public int numberOfTeams() {                       // number of teams
        return n;
    }
    public Iterable<String> teams() {                               // all teams
        return s.keySet();
    }
    public int wins(String team) {                     // number of wins for given team
        validate(team);
        return w[s.get(team)];
    }
    public int losses(String team) {                   // number of losses for given team
        validate(team);
        return l[s.get(team)];
    }
    public int remaining(String team) {                // number of remaining games for given team
        validate(team);
        return r[s.get(team)];
    }
    public int against(String team1, String team2) {   // number of remaining games between team1 and team2
        validate(team1);
        validate(team2);
        return a[s.get(team1)][s.get(team2)];
    }
    public boolean isEliminated(String team) {             // is given team eliminated?
        validate(team);
        return certificateOfElimination(team) != null;
    }
    private int min (Iterable<Integer> iterable) {
        int min = Integer.MAX_VALUE;
        for(int i : iterable) {
            if (i < min) {
                min = i;
            }
        }
        return  min;
    }
    public Iterable<String> certificateOfElimination(String team) { // subset R of teams that eliminates given team; null if not eliminate
        validate(team);

        int v = s.get(team);
        if (cache.get(v) != null) {
            if (cache.get(v).size() == 0) {
                return null;
            }
            else {
                return cache.get(v);
            }
        }

        int wins = w[v] + r[v];
        LinkedList<String> result = new LinkedList<>();

        if (wins >= maxWins) {
            int gs = 1 + countMathces + n + 1;
            int cm = countMathces + 1;
            int[][] g = new int[gs][gs];
            int k = 1;
            int maxval = Integer.MAX_VALUE / 2;
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < i; j++) {
                    if (v != i && v != j && a[i][j] > 0) {
                        g[0][k] = a[i][j];
                        g[k][cm + i] = maxval;
                        g[k][cm + j] = maxval;
                        k++;
                    }
                }
                g[cm + i][gs - 1] = wins - w[i];
            }

            int[] stack = new int[gs];
            int[] parent = new int[gs];
            int[] be = new int[gs];
            int[] path = new int[gs]; // arralist
            int stackSize = 0;
            int pathSize = 0;
            while (true) {
                for (int i = 0; i < gs; i++) {
                    be[i] = 0;
                }
                stack[0] = 0;
                stackSize = 1;
                pathSize = 0;
                be[0] = 1;
                boolean fl = true;
                while (stackSize > 0) {
                    int ver = stack[--stackSize];
                    if (ver == gs - 1) {
                        fl = false;
                        while(ver != 0) {
                            path[pathSize++] = ver;
                            ver = parent[ver];
                        }
                        break;
                    }
                    for (int i = gs - 1; i > 0; i--) {
                        if (g[ver][i] > 0 && be[i] == 0) {
                            stack[stackSize++] = i;
                            parent[i] = ver;
                            be[i] = 1;
                        }
                    }
                }
                if (fl) {
                    break;
                }
                path[pathSize++] = 0;
                int min = Integer.MAX_VALUE;
                for (int i = 0; i < pathSize - 1; i++) {
                    min = Math.min(min, g[path[i + 1]][path[i]]);
                }
                for (int i = 0; i < pathSize - 1; i++) {
                    g[path[i + 1]][path[i]] -= min;
                    g[path[i]][path[i + 1]] += min;
                }
            }
            int res = 0;
            for (int i = 1; i < cm; i++) {
                if (g[0][i] > 0 && i != v) {
                    res+=g[0][i];
                }
            }

            for (int i = 0; i < gs; i++) {
                be[i] = 0;
            }
            stack[0] = 0;
            stackSize = 1;
            be[0] = 1;
            while (stackSize > 0) {
                int ver = stack[--stackSize];
                if (ver >= cm && ver != v && ver < gs - 1) {
                    result.add(names[ver-cm]);
                }
                for (int i = gs - 1; i > 0; i--) {
                    if (g[ver][i] > 0 && be[i] == 0) {
                        stack[stackSize++] = i;
                        parent[i] = ver;
                        be[i] = 1;
                    }
                }
            }
            cache.set(v, result);
            if (result.size() == 0) {
                return null;
            }
            return result;
        }
        else {
            for (int i = 0; i < n; i++) {
                if (i != v && w[i] > w[v] + r[v] ) {
                    result.add(names[i]);
                }
            }
            cache.set(v, result);
            return result;
        }
    }

    public static void main(String[] args) {
        BaseballElimination d = new BaseballElimination("teams60.txt");
        for (int i = 0; i < 1; i++) {
            for (String team : d.teams()) {
                if (d.isEliminated(team)) {
                    StdOut.print(team + " is eliminated by the subset R = { ");
                    for (String t : d.certificateOfElimination(team)) {
                        StdOut.print(t + " ");
                    }
                    StdOut.println("}");
                } else {
                    StdOut.println(team + " is not eliminated");
                }
            }
        }
    }
}




