package wordnet;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;

/**
 * Created by User on 11/20/2015.
 */
public class Outcast {
    private WordNet wn;
    public Outcast(WordNet wordnet) {        // constructor takes a WordNet object
        wn = wordnet;
    }

    public String outcast(String[] nouns) {// given an array of WordNet nouns, return an outcast
        int max = -1;
        String s = "";
        for (int i = 0; i < nouns.length; i++) {
            int dist = 0;
            for (int j = 0; j < nouns.length; j++) {
                dist += wn.distance(nouns[i], nouns[j]);
            }
            if (dist > max) {
                max = dist;
                s = nouns[i];
            }
        }
        return s;
    }

    public static void main(String[] args) { // see test client below
        WordNet wordnet = new WordNet("synsets.txt", "hypernyms.txt");
        Outcast outcast = new Outcast(wordnet);
        In in = new In("outcast8.txt");
        String[] nouns = in.readAllStrings();
        StdOut.println(": " + outcast.outcast(nouns));
    }
}
