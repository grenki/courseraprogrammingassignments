package wordnet;

import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

import java.util.*;

/**
 * Created by User on 11/20/2015.
 */
public class SAP {

    private final Digraph d;

    // constructor takes a digraph (not necessarily a DAG)
    public SAP(Digraph G) {
        d = new Digraph(G.V());
        for (int i = 0; i < G.V(); i++) {
            for(int j: G.adj(i)) {
                d.addEdge(i, j);
            }
        }
    }
    
    // length of shortest ancestral path between v and w; -1 if no such path
    private void validate(int i) {
        if (i < 0 || i >= d.V()) {
            throw new java.lang.IndexOutOfBoundsException();
        }
    }
    private void validate2(Iterable<Integer> a) {
        if (a == null) {
            throw new java.lang.NullPointerException();
        }
    }
    private class Search {
        public int length = -1;
        public int ancestor = -1;
        Iterable<Integer> a;
        Iterable<Integer> b;

        void search() {
            LinkedList<Integer> stack = new LinkedList<>();
            HashMap<Integer, Integer> aMap = new HashMap<Integer, Integer>();
            HashMap<Integer, Integer> bMap = new HashMap<Integer, Integer>();
            for(int i : a) {
                validate(i);
                aMap.put(i, 0);
                stack.addLast(i);
            }

            for(int i : b) {
                validate(i);
                bMap.put(i, 0);
                stack.addLast(i);
            }
            while(stack.size() > 0) {
                int v = stack.pollFirst();
                if (aMap.containsKey(v) && bMap.containsKey(v)) {
                    if (length == -1 || length > aMap.get(v) + bMap.get(v)) {
                        ancestor = v;
                        length = aMap.get(v) + bMap.get(v);
                    }
                }
                boolean inA = aMap.containsKey(v);
                for(int i: d.adj(v)) {
                    if (aMap.containsKey(v)) {
                        if (!aMap.containsKey(i)) {
                            aMap.put(i, aMap.get(v) + 1);
                            stack.addLast(i);
                        }
                    }
                    if(bMap.containsKey(v)) {
                        if (!bMap.containsKey(i)) {
                            bMap.put(i, bMap.get(v) + 1);
                            stack.addLast(i);
                        }
                    }
                }
            }
        }
        public Search(int v, int w) {
            a = new LinkedList<Integer>();
            ((LinkedList<Integer>) a).add(v);
            b = new LinkedList<Integer>();
            ((LinkedList<Integer>) b).add(w);
            search();
        }
        public Search(Iterable<Integer> a, Iterable<Integer> b) {
            validate2(a);
            validate2(b);
            this.a = a;
            this.b = b;
            search();
        }
    }
    public int length(int v, int w) {
        return new Search(v, w).length;
    }

    // a common ancestor of v and w that participates in a shortest ancestral path; -1 if no such path
    public int ancestor(int v, int w) {
        return new Search(v, w).ancestor;
    }

    // length of shortest ancestral path between any vertex in v and any vertex in w; -1 if no such path
    public int length(Iterable<Integer> v, Iterable<Integer> w) {
        return new Search(v, w).length;
    }

    // a common ancestor that participates in shortest ancestral path; -1 if no such path
    public int ancestor(Iterable<Integer> v, Iterable<Integer> w) {
        return new Search(v, w).ancestor;
    }

    // do unit testing of this class
    public static void main(String[] args) {
        In in = new In("digraph-wordnet.txt");
        Digraph G = new Digraph(in);
        SAP sap = new SAP(G);
        LinkedList<Integer> q1 = new LinkedList<>();
        LinkedList<Integer> q2 = new LinkedList<>();
        q1.add(5);
        //q1.add(100000000);
        q2.add(3);
        System.out.println(sap.length(q1, q2));
        System.out.println("end");
        System.out.println(sap.length(1,3));


        G.addEdge(1,3);

        System.out.println(sap.length(1,3));
        In in2 = new In("digraph4.txt");
        Digraph G1 = new Digraph(in2);
        SAP sap2 = new SAP(G1);

        while (!StdIn.isEmpty()) {
            int v = StdIn.readInt();
            int w = StdIn.readInt();
            int length = sap.length(v, w);
            int ancestor = sap.ancestor(v, w);
            StdOut.printf("length = %d, ancestor = %d\n", length, ancestor);
        }
    }
}