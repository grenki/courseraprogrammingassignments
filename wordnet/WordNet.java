package wordnet;

import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.MinPQ;

import java.util.*;
import java.util.stream.Stream;

public class WordNet {
    private ArrayList<Synset> list = new ArrayList<>();
    private SAP sap;
    private HashMap<String, ArrayList<Integer>> hm = new HashMap<>();

    private void validate (int i) {
        if (i >= list.size() || i < 0) {
            throw new java.lang.IllegalArgumentException();
        }
    }
    private ArrayList<Integer> find(String s) {
        validate2(s);
        return hm.containsKey(s) ? (ArrayList<Integer>) hm.get(s) : null;
    }
    private class Synset {
        int id;
        String alNames;
        HashSet<String> names = new HashSet<>();
        String gloss;
        public Synset(String[] s) {
            this.id = Integer.parseInt(s[0]);
            this.alNames = s[1];
            for(String i : s[1].split(" ")) {
                names.add(i);
                if (!hm.containsKey(i)) {
                    ArrayList<Integer> al = new ArrayList<>();
                    al.add(id);
                    hm.put(i, al);
                }
                else {
                    hm.get(i).add(id);
                }
            }
            this.gloss = s[2];
        }
    }
    
    // constructor takes the name of the two input files
    public WordNet(String synsets, String hypernyms) {
        In inSynsets = new In(synsets);
        In inHypernyms = new In(hypernyms);
        while (!inSynsets.isEmpty()) {
            String s= inSynsets.readLine();
            list.add(new Synset(s.split(",")));
        }

        Digraph d = new Digraph(list.size());
        Digraph d2 = new Digraph(list.size());
        while(!inHypernyms.isEmpty()) {
            String[] s = inHypernyms.readString().split(",");
            int v = Integer.parseInt(s[0]);
            for (int i = 1; i < s.length; i++) {
                int k = Integer.parseInt(s[i]);
                d.addEdge(v, k);
                d2.addEdge(k, v);
            }
        }
        sap = new SAP(d);
    }


    // returns all WordNet nouns
    public Iterable<String> nouns() {
        HashSet<String> nouns = new HashSet<>(list.size());
        for (int i = 0; i < list.size(); i++) {
            for(String s : list.get(i).names) {
                nouns.add(s);
            }
        }
        return nouns;
    }

    // is the word a WordNet noun?
    public boolean isNoun(String word) {
        return find(word) != null;
    }
    // distance between nounA and nounB (defined below)
    private void validate2(Object o) {
        if (o == null) {
            throw new NullPointerException();
        }
    }
    public int distance(String nounA, String nounB) {
        ArrayList<Integer> i = find(nounA);
        ArrayList<Integer> j = find(nounB);
        if(i == null || j == null) {
            throw  new IllegalArgumentException();
        }
        return sap.length(i, j);
    }

    // a synset (second field of synsets.txt) that is the common ancestor of nounA and nounB
    // in a shortest ancestral path (defined below)
    public String sap(String nounA, String nounB) {
        ArrayList<Integer> i = find(nounA);
        ArrayList<Integer> j = find(nounB);
        if(i == null || j == null) {
            throw  new IllegalArgumentException();
        }
        return list.get(sap.ancestor(find(nounA), find(nounB))).alNames;
    }

    // do unit testing of this class
    public static void main(String[] args) {
        WordNet w = new WordNet("synsets100-subgraph.txt", "hypernyms100-subgraph.txt");
        System.out.println(w.find("security_blanket"));
        System.out.println(w.distance("thing", "security_blanket"));
    }
}