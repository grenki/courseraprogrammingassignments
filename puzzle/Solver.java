//package puzzle;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.MinPQ;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
public class Solver {
    private int moves = -2;
    private LinkedList<Board> solution = new LinkedList<>();
    private class Item {
        Board b;
        int pr;
        int moves;
        Item parent;

        public Item(Board b, int moves, Item parent) {
            this.b = b;
            this.moves = moves;
            pr = moves + b.manhattan();
            this.parent = parent;
        }
    }
    public Solver(Board init) {          // find a solution to the initial board (using the A* algorithm)
        Comparator<Item> comparator = new Comparator<Item>() {
            @Override
            public int compare(Item o1, Item o2) {
                return o1.pr == o2. pr ? Integer.compare(o1.moves, o2.moves)
                        : Integer.compare(o1.pr, o2.pr);
            }
        };
        MinPQ<Item> pq = new MinPQ<>(100, comparator);
        MinPQ<Item> pq2 = new MinPQ<>(100, comparator);
        pq.insert(new Item(init, 0, null));
        pq2.insert(new Item(init.twin(), 0, null));

        while (moves == -2) {
            Item item = pq.delMin();
            if (item.b.isGoal()) {
                while (item != null) {
                    solution.addFirst(item.b);
                    item = item.parent;
                }
                moves = solution.size() - 1;
                break;
            }
            for (Board i : item.b.neighbors()) {
                if (item.parent == null || !i.equals(item.parent.b)) {
                    pq.insert(new Item(i, item.moves + 1, item));
                }
            }

            item = pq2.delMin();
            if (item.b.isGoal()) {
                moves = -1;
                break;
            }
            for (Board i : item.b.neighbors()) {
                if (item.parent == null || !i.equals(item.parent.b)) {
                    pq2.insert(new Item(i, item.moves + 1, item));
                }
            }
        }
    }
    public boolean isSolvable() {           // is the initial board solvable?
        return moves > -1;
    }
    public int moves() {                    // min number of moves to solve initial board; -1 if unsolvable
        return moves;
    }
    public Iterable<Board> solution() {     // sequence of boards in a shortest solution; null if unsolvable
        if (moves < 0) {
            return null;
        }
        return solution;
    }
    public static void main(String[] args) { // solve a slider puzzle (given below)
    }
}

