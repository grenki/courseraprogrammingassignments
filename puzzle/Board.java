//package puzzle;
import edu.princeton.cs.algs4.In;

import java.util.LinkedList;
/**
 * Created by User on 11/23/2015.
 */
public class Board {
    private int n;
    private int[][] a;
    private boolean isGoal;
    private int hamming = -1;
    private int manhattan = -1;
    public Board(int[][] blocks) {          // construct a board from an N-by-N array of blocks
        // (where blocks[i][j] = block in row i, column j)
        n = blocks.length;
        a = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = blocks[i][j];
            }
        }
        isGoal = hamming() == 0;
    }
    public int dimension() {              // board dimension N
        return n;
    }
    public int hamming() {                  // number of blocks out of place
        if (hamming < 0) {
            int res = 0;
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    if (a[i][j] > 0 && a[i][j] != i * n + j + 1) {
                        res++;
                    }
                }
            }
            hamming = res;
            return res;
        }
        else {
            return hamming;
        }
    }
    public int manhattan() {                // sum of Manhattan distances between blocks and goal
        if (manhattan < 0) {
            int res = 0;
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    if (a[i][j] > 0 && a[i][j] != i * n + j + 1) {
                        res += Math.abs((a[i][j] - 1) / n - i) + Math.abs((a[i][j] - 1) % n - j);
                    }
                }
            }
            manhattan = res;
            return res;
        }
        else {
            return manhattan;
        }
    }
    public boolean isGoal() {               // is this board the goal board?
        return isGoal;
    }
    public Board twin() {                   // a board that is obtained by exchanging any pair of blocks
        Board res;
        if (a[0][0] * a[0][1] != 0) {
            swap(0, 0, 0, 1);
            res = new Board(a);
            swap(0, 0, 0, 1);
            return res;
        }
        else {
            swap(1, 0, 1, 1);
            res = new Board(a);
            swap(1, 0, 1, 1);
            return res;
        }
    }
    public boolean equals(Object y) {       // does this board equal y?
        return y != null && toString().equals(y.toString());
    }
    private void swap(int i, int j, int x, int y) {
        int k = a[i][j];
        a[i][j] = a[x][y];
        a[x][y] = k;
    }
    public Iterable<Board> neighbors() {    // all neighboring boards
        int i0 = -1;
        int j0 = -1;
        for (int i = 0; i < n && i0 == -1; i++) {
            for (int j = 0; j < n && i0 == -1; j++) {
                if (a[i][j] == 0) {
                    i0 = i;
                    j0 = j;
                }
            }
        }
        LinkedList<Board> res = new LinkedList<>();
        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                if(Math.abs(i) + Math.abs(j) == 1 && i0 + i < n && i0 + i >= 0
                        && j0 + j >= 0 && j0 + j < n) {
                    swap(i0, j0, i0 + i, j0 + j);
                    res.add(new Board(a));
                    swap(i0, j0, i0 + i, j0 + j);
                }
            }
        }
        return res;
    }
    public String toString() {// string representation of this board (in the output format specified below)
        String s = "";
        s += n + "\n";
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                s += a[i][j] + " ";
            }
            s += "\n";
        }
        return s;
    }
    public static void main(String[] args) {// unit tests (not graded)
    }
}
