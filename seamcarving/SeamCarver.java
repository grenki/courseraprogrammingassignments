//package seamcarving;

import edu.princeton.cs.algs4.Picture;

import java.awt.*;
import java.util.Map;
public class SeamCarver {
    private Picture p;
    private int w = 0;
    private int h = 0;
    private void notNull(Object o) {
        if (o == null) {
            throw new NullPointerException();
        }
    }
    public SeamCarver(Picture picture) {              // create a seam carver object based on the given picture
        notNull(picture);
        p = new Picture(picture);
        w = p.width();
        h = p.height();
    }
    public Picture picture() {                         // current picture
        return new Picture(p);
    }
    public     int width()  {                          // width of current picture
        return w;
    }
    public     int height() {                          // height of current picture
        return h;
    }
    private int sq (int i) {
        return i * i;
    }
    private int delta(Color a, Color b) {
        return sq(a.getBlue() - b.getBlue()) + sq(a.getGreen() - b.getGreen()) + sq(a.getRed() - b. getRed());
    }
    public  double energy(int x, int y) {              // energy of pixel at column x and row y
        if (x < 0 || y < 0 || x >= w || y >= h) {
            throw new IndexOutOfBoundsException();
        }
        if (x == 0 || y == 0 || x == w - 1 || y == h - 1) {
            return 1000;
        }
        Color l = p.get(x - 1, y);
        Color r = p.get(x + 1, y);
        Color u = p.get(x, y - 1);
        Color d = p.get(x, y + 1);
        return Math.sqrt(delta(l, r) + delta(u, d));
    }
    private void fillEnergy (double[][] e, boolean reverse) {
        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                if (reverse) {
                    e[j][i] = energy(i, j);
                } else {
                    e[i][j] = energy(i, j);
                }
            }
        }
    }
    private int[] findSeam(boolean horizontalDirection) {  //true == horizontal
        int h;
        int w;
        double[][] e;
        if (horizontalDirection) {
            h = this.w;
            w = this.h;
        }
        else {
            h = this.h;
            w = this.w;
        }
        int[] res = new int[h];
        e = new double[w][h];
        fillEnergy(e, horizontalDirection);

        if (w == 1) {
            for (int i = 0; i < h; i++) {
                return res;
            }
        }
        int[][] parent = new int[w][h];
        int k;
        for (int j = 1; j < h; j++) {
            for (int i = 0; i < w; i++) { /// !!!!! Attention
                if (i == 0) {
                    k = 1;
                }
                else if (i == w - 1) {
                    k = -1;
                }
                else {
                    k = min(e[i - 1][j - 1], e[i][j - 1], e[i + 1][j - 1]);
                }
                e[i][j] += e[i + k][j - 1];
                parent[i][j] = k;
            }
        }
        Double min = Double.POSITIVE_INFINITY;
        int minV = -1;
        for (int i = 1; i < w - 1; i++) {  /// Potential BUG
            if (min > e[i][h-1]) {
                min = e[i][h - 1];
                minV = i;
            }
        }
        int i = minV;
        for (int j = h - 1; j > -1; j--) {
            res[j] = i;
            try {
                i += parent[i][j];
            }
            catch (ArrayIndexOutOfBoundsException exc) {
                System.out.println(w + " " + h  + " " + i + " " + j);
            }
        }
        return res;

    }
    public   int[] findHorizontalSeam() {              // sequence of indices for horizontal seam
        return findSeam(true);
    }

    private int min (double a, double b, double c) {
        if (a < b) {
            if (a < c) {
                return -1;
            }
            else {
                return 1;
            }
        }
        else {
            if (b > c) {
                return 1;
            }
            else {
                return 0;
            }
        }
    }
    public   int[] findVerticalSeam() {                // sequence of indices for vertical seam
        return findSeam(false);
    }
    private void validateSeam(int[] seam, boolean r) {
        if (seam == null) {
            throw new NullPointerException();
        }
        if (seam.length != (r ? w : h) || (r ? h : w) <= 1) {
            throw new IllegalArgumentException();
        }
        for (int i = 1; i < seam.length; i++) {
            if (Math.abs(seam[i] - seam[i-1]) > 1) {
                throw new IllegalArgumentException();
            }
        }
        for (int i = 0; i < seam.length; i++) {
            if(seam[i] < 0 || seam[i] >= (r ? h : w)) {
                throw new IllegalArgumentException();
            }
        }
    }
    private void removeSeam(int[] seam, boolean r) {
        validateSeam(seam, r);
        Picture p2;
        if (r) {
            p2 = new Picture(w, h - 1);
        }
        else {
            p2 = new Picture(w - 1, h);
        }
        for (int i = 0; i < p2.width(); i++) {
            for (int j = 0; j < p2.height(); j++) {
                if ((!r && i < seam[j]) || (r && j < seam[i])) {
                    p2.set(i, j, p.get(i, j));
                }
                else {
                    if (r) {
                        p2.set(i, j, p.get(i, j + 1));
                    }
                    else {
                        p2.set(i, j, p.get(i + 1, j));
                    }
                }
            }
        }
        p = p2;
        h = p.height();
        w = p.width();
    }
    public    void removeHorizontalSeam(int[] seam) {  // remove horizontal seam from current picture
        removeSeam(seam, true);
    }
    public    void removeVerticalSeam(int[] seam) {    // remove vertical seam from current picture
        removeSeam(seam, false);
    }

    public static void main(String[] args) {
        SeamCarver s = new SeamCarver(new Picture("1x8.png"));
        s.picture().show();
        s.findVerticalSeam();
        s.findHorizontalSeam();
    }
}